//
//  NetworkService.swift
//  task2
//
//  Created by Владислав on 05.03.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

import Foundation
import SystemConfiguration

class NetworkService
{
//    lazy var configuration: URLSessionConfiguration = URLSessionConfiguration.default
//    lazy var session: URLSession = URLSession(configuration: self.configuration)
//    
//    let url: URL
//    
//    init(url: URL) {
//        self.url = url
//    }
    
//    typealias ImageDataHandler = ((Data) -> Void)
    
//    func downloadImage(_ completion: @escaping ImageDataHandler)
//    {
//        let request = URLRequest(url: self.url)
//        let dataTask = session.dataTask(with: request, completionHandler: { (data, response, error) in
//            
//            if error == nil {
//                if let httpResponse = response as? HTTPURLResponse {
//                    switch (httpResponse.statusCode) {
//                    case 200:
//                        if let data = data {
//                            completion(data)
//                        }
//                    default:
//                        print(httpResponse.statusCode)
//                    }
//                }
//            } else {
//                print("Error: \(error?.localizedDescription)")
//            }
//        })
//        
//        dataTask.resume()
//    }
    
    class func getURLRequest(urlString: String) -> URLRequest? {
        
        let query_url = urlString
        
        if let urlAddressEscaped = query_url.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed) {
            
            if let urlAddressEscapedURL = URL(string: urlAddressEscaped) {
                
                let request = URLRequest(url: urlAddressEscapedURL, cachePolicy: .returnCacheDataElseLoad, timeoutInterval: 20)
                return request
                
            } else {
                return nil
            }
        } else {
            return nil
        }
    }
    
    class func isInternetAvailable() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
}

extension NetworkService
{
    static func parseJSONFromData(_ jsonData: Data?) -> [String : AnyObject]?
    {
        if let data = jsonData {
            do {
                let jsonDictionary = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String : AnyObject]
                
                
                
//                let file = "file.json" //this is the file. we will write to and read from it
//                
//                let text = jsonDictionary?.description //just a text
//                
//                if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
//                    
//                    let path = dir.appendingPathComponent(file)
//                    
//                    //writing
//                    do {
//                        try text?.write(to: path, atomically: false, encoding: String.Encoding.utf8)
//                    }
//                    catch {
//                        print("writing file failed")
//                    }
//                }
                
                
                
                return jsonDictionary
                
            } catch let error as NSError {
                print("error processing json data: \(error.localizedDescription)")
            }
        }
        
        return nil
    }
}
