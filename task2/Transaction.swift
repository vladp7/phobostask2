//
//  Transaction.swift
//  task2
//
//  Created by Владислав on 05.03.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

import Foundation
import UIKit

class Transaction {
    var id: Int?
    var status: String?
    var happened_at: Date?
    var details: String?
    var comment: String?
    var mimimiles: Float?
    
    var moneyAmount: Int?
    var moneyCurrencyCode: String?
    
    var merchantId: Int?
    var merchantName: String?
    
    var categoryId: Int?
    var categoryName: String?
    var categoryIcon: String?
    var categorySubIcon: UIImage?

    
    init(id: Int, status: String, happened_at: Int, details: String, comment: String, mimimiles: Float, money: [String : AnyObject], merchant: [String : AnyObject], category: [String : AnyObject])
    {
        self.id = id
        self.status = status
        self.happened_at = Date(timeIntervalSince1970: TimeInterval(happened_at))
        self.details = details
        self.comment = comment
        self.mimimiles = mimimiles
        self.moneyAmount = money["amount"] as! Int
        self.moneyCurrencyCode = money["curerncy_code"] as! String
//        print("moneyAmount = \(self.moneyAmount!), moneyCurrenceCode = \(self.moneyCurrencyCode!)")
        self.merchantId = merchant["id"] as! Int
        self.merchantName = merchant["name"] as! String
        
        self.categoryId = category["id"] as! Int
        self.categoryName = category["name"] as! String
        self.categoryIcon = category["icon"] as! String
        self.categorySubIcon = UIImage(named: category["sub_icon"] as! String)
//        print("(init1)category sub icon = \(category["sub_icon"] as! String)")
    }
    
    
    init(transactionDict: [String : Any]) {
        self.id = transactionDict["id"] as! Int
        self.status = String(describing: transactionDict["status"]!)
        self.happened_at = Date(timeIntervalSince1970: TimeInterval(transactionDict["happened_at"] as! Float))
        self.details = String(describing: transactionDict["details"]!)
        self.comment = String(describing: transactionDict["comment"]!)
        self.mimimiles = transactionDict["mimimiles"] as! Float
        
        let money = transactionDict["money"] as! [String : AnyObject]
        self.moneyAmount = money["amount"] as! Int
        self.moneyCurrencyCode = money["curerncy_code"] as! String
        //        print("moneyAmount = \(self.moneyAmount!), moneyCurrenceCode = \(self.moneyCurrencyCode!)")
        
        let merchant = transactionDict["merchant"] as! [String : AnyObject]
        self.merchantId = merchant["id"] as! Int
        self.merchantName = merchant["name"] as! String
        
        let category = transactionDict["category"] as! [String : AnyObject]
        self.categoryId = category["id"] as! Int
        self.categoryName = category["name"] as! String
        self.categoryIcon = category["icon"] as! String
        self.categorySubIcon = UIImage(named: category["sub_icon"] as! String)
//        print("(init2)category sub icon = \(category["sub_icon"] as! String)")
//        print("staart id = \(id), status = \(status), happened_at = \(happened_at), details = \(details), mimimiles = \(mimimiles), money = \(money), merchant = \(merchant), category = \(category) eend")
    }
}
