//
//  JSONService.swift
//  task2
//
//  Created by Владислав on 06.03.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

import Foundation

class JSONService {
    
    let jsonData = try? Data(contentsOf: URL(string: "http://mobile165.hr.phobos.work/list")!)

    init() {
    }
    
    func dates() -> [String]? {
        if let jsonDictionary = NetworkService.parseJSONFromData(jsonData) {
            
            print("json dictionary as string = \(jsonDictionary.description)")
            
            let dateDictionaries = jsonDictionary["feed"] as! [String : Any]
            let dates = Array(dateDictionaries.keys)
            return dates.sorted()
            
        } else {
            return nil
        }
    }
    
    func balance() -> String? {
        
//        let file = "file.json" //this is the file. we will write to and read from it
//        
//        //        let text = "some text" //just a text
//        
//        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
//            
//            let path = dir.appendingPathComponent(file)
//            
//            //reading
//            do {
//                let text2 = try String(contentsOf: path, encoding: String.Encoding.utf8)
//                
//                let dict = convertToDictionary(text: text2)
//                print("text2 = \(text2)")
//                print("converted string from file to dict = \(dict)")
//            }
//            catch {
//                print("reading file error")
//            }
//        }
        
        
        if let jsonDictionary = NetworkService.parseJSONFromData(jsonData) {
            let userDictionaries = jsonDictionary["user"] as! [String : Any]
            let balance = String(describing: userDictionaries["balance"]!)
            return balance
            
        } else {
            return nil
        }
    }
    
    func miles() -> String? {
        if let jsonDictionary = NetworkService.parseJSONFromData(jsonData) {
            let userDictionaries = jsonDictionary["user"] as! [String : Any]
            let miles = String(describing: userDictionaries["miles"]!)
            return miles
            
        } else {
            return nil
        }
    }
    
    func downloadAllTransactionsForDate(date: String?) -> [Transaction] {
        
        var transactions = [Transaction]()
        if let jsonDictionary = NetworkService.parseJSONFromData(jsonData) {
            let dateDictionaries = jsonDictionary["feed"] as! [String : Any]
            if let aDate = date {
                let transactionsForDate = dateDictionaries[aDate] as! [Any]
                
                for (index, _) in transactionsForDate.enumerated() {
                    let transaction = transactionsForDate[index] as! [String: Any]
                    
                    let newTransaction = Transaction(transactionDict: transaction)
                    transactions.append(newTransaction)
                }                
            }
        }
        return transactions
    }
    
    // MARK: - Custom methods
//    func convertToDictionary(text: String) -> [String: Any]? {
//        if let data = text.data(using: .utf8) {
//            do {
//                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
//            } catch {
//                print(error.localizedDescription)
//            }
//        }
//        return nil
//    }
}
