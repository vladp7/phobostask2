//
//  TransactionTableViewCell.swift
//  task2
//
//  Created by Владислав on 07.03.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

import UIKit

class TransactionTableViewCell: UITableViewCell {

    @IBOutlet var idLbl: UILabel!
    @IBOutlet var statusLbl: UILabel!
    @IBOutlet var dateAndTimeLbl: UILabel!
    @IBOutlet var detailsLbl: UILabel!
    @IBOutlet var commentLbl: UILabel!
    @IBOutlet var mimimilesLbl: UILabel!
    @IBOutlet var moneyAmountLbl: UILabel!
    @IBOutlet var moneyCurrencyCodeLbl: UILabel!
    @IBOutlet var merchantIdLbl: UILabel!
    @IBOutlet var merchantNameLbl: UILabel!
    @IBOutlet var categoryIdLbl: UILabel!
    @IBOutlet var categoryNameLbl: UILabel!
    @IBOutlet var categorySubIcon: UIImageView!
    
    var transaction: Transaction! {
        didSet {
            self.updateUI()
        }
    }
    
    func updateUI()
    {
        idLbl.text = String(describing: transaction.id!)
        statusLbl.text = transaction.status
        
        let date = transaction.happened_at!
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "MMM dd YYYY hh:mm a"
        
        let dateString = dayTimePeriodFormatter.string(from: date)
        
//        print( " dateString is \(dateString)")
        
        dateAndTimeLbl.text = dateString
        detailsLbl.text = transaction.details
        commentLbl.text = transaction.comment
        mimimilesLbl.text = String(describing: transaction.mimimiles!)
        moneyAmountLbl.text = String(describing: transaction.moneyAmount!)
        moneyCurrencyCodeLbl.text = transaction.moneyCurrencyCode
        merchantIdLbl.text = String(describing: transaction.merchantId!)
        merchantNameLbl.text = transaction.merchantName
        categoryIdLbl.text = String(describing: transaction.categoryId!)
        categoryNameLbl.text = transaction.categoryName
//        categorySubIcon.image = UIImage(named: transaction.categorySubIcon!)
        categorySubIcon.image = transaction.categorySubIcon
        

    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
