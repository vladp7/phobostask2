//
//  MainViewController.swift
//  task2
//
//  Created by Владислав on 06.03.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var mainTableView: UITableView!
    @IBOutlet var balanceLbl: UILabel!
    @IBOutlet var milesLbl: UILabel!
    
//    let jsonService = JSONService()
    var dates = [String]()
    var refreshControl: UIRefreshControl!
    
    
//    let jsonFileURL = URL(string: "http://mobile165.hr.phobos.work/list")
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        getData()
        
        if let request = NetworkService.getURLRequest(urlString: "http://mobile165.hr.phobos.work/list") {
            
            if let cachedResponseData = URLCache.shared.cachedResponse(for: request)?.data {
                
                let json = JSON(data: cachedResponseData)
                
                self.fillData(json: json)
                
//                print("cachedResponse = \(json)")
                
            } else if NetworkService.isInternetAvailable() {
                
                alamofireRequest(request: request)
                
            } else {
                showAlert(message: "Cache and internet connection unavailable")
            }
        }
        
        refreshControl = UIRefreshControl()
//        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh), for: UIControlEvents.valueChanged)
        mainTableView.addSubview(refreshControl)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "presentTransaction" {
            if let indexPath = mainTableView.indexPathForSelectedRow {
                let destinationController = segue.destination as! TransactionViewController
                destinationController.date = dates[indexPath.row]
            }
        }
    }

    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dates.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MainTableViewCell", for: indexPath)
        cell.textLabel?.text = dates[indexPath.row]
        return cell
    }
    
    // MARK: - Custom methods
    func refresh(sender:AnyObject) {
        
        if let request = NetworkService.getURLRequest(urlString: "http://mobile165.hr.phobos.work/list") {
            
            if NetworkService.isInternetAvailable() {
                
                alamofireRequest(request: request)
                
            } else {
                showAlert(message: "Internet connection unavailable")
            }
        }
        
//        mainTableView.reloadData()
        self.refreshControl?.endRefreshing()
    }
    
    func fillData(json: JSON) {
        let balance = json["user"]["balance"].int!
        self.balanceLbl.text = String(balance)
        let miles = json["user"]["miles"].int!
        self.milesLbl.text = String(miles)
        let dates = json["feed"].dictionaryValue
        let datesKeys = Array(dates.keys)
        self.dates = datesKeys.sorted()
    }
    
    func showAlert(message: String) {
        let alert = UIAlertController(title: "Warning", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func alamofireRequest(request: URLRequest) {
        Alamofire.request(request)
            .responseJSON { (response) in
                
                let cachedURLResponse = CachedURLResponse(response: response.response!, data: response.data! as Data, userInfo: nil, storagePolicy: .allowed)
                
                URLCache.shared.storeCachedResponse(cachedURLResponse, for: response.request!)
                
                guard response.result.error == nil else {
                    
                    // got an error in getting the data, need to handle it
                    print("error fetching data from url")
                    print(response.result.error!)
                    return
                    
                }
                
                let json = JSON(data: cachedURLResponse.data)
                
                self.fillData(json: json)
                
                self.mainTableView.reloadData()
                
//                print("alamofire json = \(json)")
        }
    }
}
