//
//  TransactionViewController.swift
//  task2
//
//  Created by Владислав on 06.03.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class TransactionViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var dateLbl: UILabel!
    @IBOutlet var transactionsTableView: UITableView!
    
    let jsonService = JSONService()
    var date = ""
    var transactions = [Transaction]()
    var refreshControl: UIRefreshControl!
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dateLbl.text = date
//        
////        transactions = jsonService.downloadAllTransactionsForDate(date: date)
//        if let data = try? Data(contentsOf: URL(string: "http://mobile165.hr.phobos.work/list")!) {
////        if let uData = oData {
//            
//            let clearJSON = JSON(data: data, options: JSONSerialization.ReadingOptions.mutableContainers, error: nil)
//            
//            let dates = clearJSON["feed"].dictionaryValue
//            
//            let oChoosedDate = dates[date]?.arrayValue
//            if let uChoosedDate = oChoosedDate {
////                print("date = \(date), date count = \(uChoosedDate.count), date data = \(uChoosedDate)")
//                for (index, _) in uChoosedDate.enumerated() {
//                    if let transaction = uChoosedDate[index].dictionaryObject {
////                        print("transaction = \(transaction)")
//                        let newTransaction = Transaction(transactionDict: transaction)
//                        transactions.append(newTransaction)
//                    }
//                }
//            }
//        }
        
        if let request = NetworkService.getURLRequest(urlString: "http://mobile165.hr.phobos.work/list") {
            
            if let cachedResponseData = URLCache.shared.cachedResponse(for: request)?.data {
                
                let json = JSON(data: cachedResponseData)
                
                self.fillData(json: json)
                
//                print("cachedResponse = \(json)")
                
            } else if NetworkService.isInternetAvailable() {
                
                alamofireRequest(request: request)
                
            } else {
                showAlert(message: "Cache and internet connection unavailable")
            }
        }

        refreshControl = UIRefreshControl()
        //        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh), for: UIControlEvents.valueChanged)
        transactionsTableView.addSubview(refreshControl)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return transactions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionsTableViewCell", for: indexPath)
//        cell.textLabel?.text = transactions[indexPath.row].details
//        return cell
        
        let cell = Bundle.main.loadNibNamed("TransactionTableViewCell", owner: self, options: nil)?.first as! TransactionTableViewCell
        
        cell.transaction = transactions[indexPath.row]
        return cell
    }
    
    // MARK: - Custom methods
    func refresh(sender:AnyObject) {
        
        transactions = []
//        let oData = try? Data(contentsOf: URL(string: "http://mobile165.hr.phobos.work/list")!)
//        if let uData = oData {
//            let clearJSON = JSON(data: uData, options: JSONSerialization.ReadingOptions.mutableContainers, error: nil)
//            
//            let dates = clearJSON["feed"].dictionaryValue
//            
//            let oChoosedDate = dates[date]?.arrayValue
//            if let uChoosedDate = oChoosedDate {
//                for (index, _) in uChoosedDate.enumerated() {
//                    if let transaction = uChoosedDate[index].dictionaryObject {
//                        let newTransaction = Transaction(transactionDict: transaction)
//                        transactions.append(newTransaction)
//                    }
//                }
//            }
//        }
        
        if let request = NetworkService.getURLRequest(urlString: "http://mobile165.hr.phobos.work/list") {
            
            if NetworkService.isInternetAvailable() {
                
                alamofireRequest(request: request)
                
            } else {
                showAlert(message: "Internet connection unavailable")
            }
        }
        
//        transactionsTableView.reloadData()
        self.refreshControl?.endRefreshing()
    }
    
    func fillData(json: JSON) {
        
        let dates = json["feed"].dictionaryValue
        
        if let choosedDate = dates[date]?.arrayValue {
            //                print("date = \(date), date count = \(uChoosedDate.count), date data = \(uChoosedDate)")
            for (index, _) in choosedDate.enumerated() {
                if let transaction = choosedDate[index].dictionaryObject {
                    //                        print("transaction = \(transaction)")
                    let newTransaction = Transaction(transactionDict: transaction)
                    transactions.append(newTransaction)
                }
            }
        }
    }
    
    func alamofireRequest(request: URLRequest) {
        Alamofire.request(request)
            .responseJSON { (response) in
                
                let cachedURLResponse = CachedURLResponse(response: response.response!, data: response.data! as Data, userInfo: nil, storagePolicy: .allowed)
                
                URLCache.shared.storeCachedResponse(cachedURLResponse, for: response.request!)
                
                guard response.result.error == nil else {
                    
                    // got an error in getting the data, need to handle it
                    print("error fetching data from url")
                    print(response.result.error!)
                    return
                    
                }
                
                let json = JSON(data: cachedURLResponse.data)
                
                self.fillData(json: json)
                
                self.transactionsTableView.reloadData()
                
//                print("alamofire json = \(json)")
                
        }
    }
    
    func showAlert(message: String) {
        let alert = UIAlertController(title: "Warning", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
